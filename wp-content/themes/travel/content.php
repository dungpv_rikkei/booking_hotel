<?php if ($loop->have_posts()) : $i = 0; ?>
    <?php while ($loop->have_posts()) : $loop->the_post();
        global $post; ?>
        <?php if ($i % 2 == 0): echo '<div class="row row-post">'; endif; ?>
        <div class="col-md-6  col-sm-6 col-xs-12 element_article">
            <div>
                <div class="logo">
                    <a href="<?php the_permalink() ?>">
                        <img onerror="this.src='<?php echo esc_url(get_template_directory_uri()) . '/images/thumbnail-default.jpg'; ?>'"
                             src="<?php $src = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                             if (!empty($src)) echo $src ?>" alt="" class="img img-responsive img-thumb-post">
                    </a>

                    <?php
                    $meta_flag = '';
                    $meta_bg = '';
                    $meta_icon = '';
                    if ('holiday' == get_post_type()) {
                        $meta_flag = get_post_meta($post->ID, 'wpcf-holiday_notification_text', true);
                        $meta_bg = get_post_meta($post->ID, 'wpcf-holiday_notification_bckgr', true);
                        $meta_icon = get_post_meta($post->ID, 'wpcf-holiday_notification_icon', true);
                    } else if ('flight' == get_post_type()) {
                        $meta_flag = get_post_meta($post->ID, 'wpcf-flight_notification_text', true);
                        $meta_bg = get_post_meta($post->ID, 'wpcf-flight_notification_bckgr', true);
                        $meta_icon = get_post_meta($post->ID, 'wpcf-flight_notification_icon', true);
                    } else if ('accomodation' == get_post_type()) {
                        $meta_flag = get_post_meta($post->ID, 'wpcf-acc_notification_text', true);
                        $meta_bg = get_post_meta($post->ID, 'wpcf-acc_notification_bckgr', true);
                        $meta_icon = get_post_meta($post->ID, 'wpcf-acc_notification_icon', true);
                    } else if ('cruise' == get_post_type()) {
                        $meta_flag = get_post_meta($post->ID, 'wpcf-cruise_notification_text', true);
                        $meta_bg = get_post_meta($post->ID, 'wpcf-cruise_notification_bckgr', true);
                        $meta_icon = get_post_meta($post->ID, 'wpcf-cruise_notification_icon', true);
                    } else if ('other' == get_post_type()) {
                        $meta_flag = get_post_meta($post->ID, 'wpcf-other_notification_text', true);
                        $meta_bg = get_post_meta($post->ID, 'wpcf-other_notification_bckgr', true);
                        $meta_icon = get_post_meta($post->ID, 'wpcf-other_notification_icon', true);
                    }
                    ?>
                    <?php if (!empty($meta_flag)): ?>
                        <div class="flag_article" style="background-color:<?php echo $meta_bg ?> ">
                            <?php if (!empty($meta_icon)): ?>
                                <span class="<?php echo $meta_icon ?> error-fare" aria-hidden="true"></span>
                            <?php endif;
                            echo $meta_flag ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="info">
                    <div class="row">
                        <div class="col-sm-7">
                            <h3 class="title_thumbnail"><a
                                        href="<?php the_permalink() ?>"><?php wp_trim_words(the_title(), 30) ?></a></h3>
                            <p class="des-item">
                                <?php echo wp_trim_words(get_the_excerpt(), 29); ?>
                            </p>
                        </div>
                        <?php $totalPrice = 0; ?>
                        <div class="col-sm-5 detail-book-item col-xs-12">
                            <div class="row_icon2">
                                                    <span class="fa fa-plane icon3" aria-hidden="true">
                                                        <strong>
                                                            <?php
                                                            $meta_values = '';
                                                            if ('holiday' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-holiday_flight_price', true);
                                                            } else if ('flight' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-flight_price', true);
                                                            } else if ('accomodation' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-acc_flight_price', true);
                                                            } else if ('cruise' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-cruise_flight_price', true);
                                                            } else if ('other' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-other_flight_price', true);
                                                            }
                                                            echo $meta_values ? '$' . $meta_values : '$0';
                                                            ?></strong>
                                                    </span>


                                <span class="fa fa-calendar icon2" aria-hidden="true">
                                                        <strong><?php
                                                            $meta_values = '';
                                                            if ('holiday' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-holiday_date', true);
                                                            } else if ('flight' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-flight_date', true);
                                                            } else if ('accomodation' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-acc_date', true);
                                                            } else if ('cruise' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-cruise_date', true);
                                                            } else if ('other' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-other_date', true);
                                                            }
                                                            echo $meta_values ? strtoupper(date("d.M.y", $meta_values)) : '';
                                                            ?>
                                                        </strong>
                                </span>

                            </div>
                            <div class="row_icon1">
                                                    <span class="fa fa-user-o icon1" aria-hidden="true">
                                                        <strong>
                                                            <?php
                                                            $meta_values = '';
                                                            if ('holiday' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-holiday_people_number', true);
                                                            } else if ('flight' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-flight_people_number', true);
                                                            } else if ('accomodation' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-acc_people_number', true);
                                                            } else if ('cruise' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-cruise_people_number', true);
                                                            } else if ('other' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-other_people_number', true);
                                                            }
                                                            echo $meta_values ? '$'.$meta_values : '0';
                                                            ?>
                                                        </strong>
                                                    </span>
                                <span class="fa fa-university icon4" aria-hidden="true">
                                                        <strong><?php
                                                            if ('holiday' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-holiday_acc_price', true);
                                                            } else if ('flight' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-flight_acc_price', true);
                                                            } else if ('accomodation' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-acc_price', true);
                                                            } else if ('cruise' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-cruise_acc_price', true);
                                                            } else if ('other' == get_post_type()) {
                                                                $meta_values = get_post_meta($post->ID, 'wpcf-other_acc_price', true);
                                                            }
                                                            echo $meta_values ? '$' . $meta_values : 'NO';
                                                            ?></strong>
                                                    </span>
                            </div>
                            <hr class="hr_element">
                            <?php if ('cruise' == get_post_type()) {
                                $totalPrice += intval(get_post_meta($post->ID, 'wpcf-cruise_price', true));
                            } else if ('other' == get_post_type()) {
                                $totalPrice += intval(get_post_meta($post->ID, 'wpcf-other_price', true));
                            }else if ('holiday' == get_post_type()) {
                                $totalPrice += intval(get_post_meta($post->ID, 'wpcf-holiday_price', true));
                            }else if ('flight' == get_post_type()) {
                                $totalPrice += intval(get_post_meta($post->ID, 'wpcf-flight_price', true));
                            }else if ('accomodation' == get_post_type()) {
                                $totalPrice += intval(get_post_meta($post->ID, 'wpcf-acc_price', true));
                            }

                            ?>
                            <h3 class="total_money">Total: $<?php echo $totalPrice ?></h3>
                            <a class="btn btn-booking col-xs-12" href="<?php the_permalink() ?>">BOOK NOW!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if ($i % 2 != 0 || ($i == $loop->post_count - 1)): echo '</div>'; endif;
        $i++; ?>
    <?php endwhile; ?>
    <?php include(locate_template('pagination.php')); ?>
<?php endif; ?>
