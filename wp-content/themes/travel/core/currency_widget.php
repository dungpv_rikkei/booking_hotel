<?php

// Creating the widget
class currency_widget extends WP_Widget
{

    function __construct()
    {
        parent::__construct(
// Base ID of your widget
            'currency_widget',
// Widget name will appear in UI
            __('Multi currency', 'currency_widget_domain'),
// Widget description
            array('description' => __('Widget multi currency', 'currency_widget_domain'),)
        );
    }

// Creating widget front-end
// This is where the action happens
    public function widget($args, $instance)
    {
        if (!isset($_SESSION['currency'])) {
            $_SESSION['currency'] = null;
        }
        $taxonomy = 'currency';
        $taxonomy_terms = get_terms($taxonomy, array(
            'hide_empty' => 0,
        ));
        $currencySelected = null;
        $listCurrency = array();
        foreach ($taxonomy_terms as $term) {
            $termQuery = get_term_meta($term->term_id);
            if (!$_SESSION['currency']) {
                $currencySelected = $termQuery;
                $_SESSION['currency'] = $currencySelected['wpcf-unit'][0];
            } else {
                if ($_SESSION['currency'] != trim($termQuery['wpcf-unit'][0])) {
                    array_push($listCurrency, $termQuery);
                } else {
                    $currencySelected = $termQuery;
                }
            }
        }
        if (empty($listCurrency)) {
            return;
        }
        ?>
        <li>
            <a href="#" unit="<?php echo $currencySelected['wpcf-unit'][0] ?>">
                <img src="<?php echo $currencySelected['wpcf-icon'][0] ?>"
                     class="img-icon-money img img-circle img-responsive" alt=""/>
                <span><?php echo $currencySelected['wpcf-unit'][0] ?></span>
            </a>
        </li>
        <div class='hidden' id='list-money-container'>
            <ul class='list-national'>
                <?php foreach ($listCurrency as $currency): ?>
                    <li>
                        <a class="currency_item"
                           href="<?php get_site_url() ?>?cur=<?php echo $currency['wpcf-unit'][0] ?>"
                           unit="<?php echo $currency['wpcf-unit'][0] ?>">
                            <img src="<?php echo $currency['wpcf-icon'][0] ?>" class="img img-circle img-responsive"
                                 alt=""/>
                            <span><?php echo $currency['wpcf-unit'][0] ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php

    }


// Widget Backend
    public function form($instance)
    {

    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
} // Class wpb_widget ends here

register_widget('currency_widget');

