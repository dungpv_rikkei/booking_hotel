<?php

// Creating the widget
class wpb_widget extends WP_Widget
{

    function __construct()
    {
        parent::__construct(
// Base ID of your widget
            'wpb_widget',
// Widget name will appear in UI
            __('Multi language', 'wpb_widget_domain'),
// Widget description
            array('description' => __('Widget multi language', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end
// This is where the action happens
    public function widget($args, $instance)
    {
        pll_the_languages([
            'show_flags' => 1,
            'show_names' => true,
            'hide_if_empty' => 0,
            'display_names_as' => 'slug', // valid options are slug and name
        ]);
        ?>
        <div class='hidden' id='list-national-container'>
            <ul class='list-national'>
                <?php
                pll_the_languages([
                    'show_flags' => 1,
                    'show_names' => true,
                    'hide_if_empty' => 0,
                    'hide_current' => true,
                    'display_names_as' => 'name', // valid options are slug and name
                ]);
                ?>
            </ul>
        </div>
       <?php
    }

// Widget Backend
    public function form($instance)
    {
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget()
{
    register_widget('wpb_widget');
}
add_action('widgets_init', 'wpb_load_widget');

