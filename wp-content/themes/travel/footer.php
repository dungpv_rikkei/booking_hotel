<div id="footer">
    <div class="container-fluid container-footer">
        <div class="row row-footer">
            <div class="col-md-3 left-footer">
               <div class="row-fluid">
                   <h3 id="title-footer">Title</h3>
                   <p class="text-justify title-footer-des">
                       Integer ornare libero nisi. Duis ac magna urna. Nulla facilisi Phasellus at ante magna Quisque nec porta nulla Suspendisse nec orci vel elit aliquet blandit.
                   </p>
               </div>
            </div>
            <div class="col-md-9 col-xs-12 footer-menu">
                <div class="row-fluid row-left-footer">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <h4 class="title-header-menu">About Us</h4>
                        <ul class="list-unstyled list-item-footer">
                            <li><a href="">Dashboard</a></li>
                            <li><a href="">Feeds</a></li>
                            <li><a href="">Forums</a></li>
                            <li><a href="">Store</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <h4 class="title-header-menu">Categories</h4>
                        <ul class="list-unstyled list-item-footer">
                            <li><a href="">Web Designs</a></li>
                            <li><a href="">Photography</a></li>
                            <li><a href="">Digital Art</a></li>
                            <li><a href="">Vector Art</a></li>
                            <li><a href="">Vector Art</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3  col-xs-6">
                        <h4 class="title-header-menu">Networks</h4>
                        <ul class="list-unstyled list-item-footer">
                            <li><a href="">Other Sites</a></li>
                            <li><a href="">Insight</a></li>
                            <li><a href="">Planing</a></li>
                            <li><a href="">Production</a></li>
                            <li><a href="">Store</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <h4 class="title-header-menu">Follow Us</h4>
                        <ul class="list-unstyled list-item-footer">
                            <li><a href="">Facebook</a></li>
                            <li><a href="">Twitter</a></li>
                            <li><a href="">LikedIn</a></li>
                            <li><a href="">Rss Feed</a></li>
                            <li><a href="">Youtube</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/select2.full.min.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>
