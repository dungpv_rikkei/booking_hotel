<div class="search from-search">
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-12 col-md-11 col-md-offset-1">
                <form action=""  id="form-search" class="form-inline">
                    <div class="container-form row">
                        <div class="form-group form-item has-feedback">
                            <select style="width: 220px" name="departure_id[]" id="departure" multiple
                                    class="form-control">
                                <?php $listDeparture = getTaxonomy('departure'); ?>
                                <?php foreach ($listDeparture as $depart): ?>
                                    <?php $isExist = false;
                                    foreach ($departureId as $i => $de) {
                                        if ($depart->term_id == $de) {
                                            $isExist = true;
                                            break;
                                        }
                                    } ?>
                                    <option <?php if ($isExist) echo "selected"; ?> style="display: none"
                                                                                    value="<?php echo $depart->term_id ?>"><?php echo $depart->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span class="fa fa-map-marker form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group form-item has-feedback">
                            <select style="width: 220px" name="destination_id[]" id="destination" multiple
                                    class="form-control">
                                <?php $listDestination = getTaxonomy('destination'); ?>
                                <?php foreach ($listDestination as $destination): ?>
                                    <?php $isExist = false;
                                    foreach ($destinationId as $i => $des) {
                                        if ($destination->term_id == $des) {
                                            $isExist = true;
                                            break;
                                        }
                                    } ?>
                                    <option <?php if ($isExist) echo "selected"; ?> style="display: none"
                                                                                    value="<?php echo $destination->term_id ?>"><?php echo $destination->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span class="fa fa-map-marker form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group form-item has-feedback">
                            <select style="width: 220px" name="date_from[]" id="date_from" multiple class="form-control form-custom">
                                <?php $listDate = getListDateTravel(); ?>
                                <option value=""></option>
                                <?php foreach ($listDate as $index => $date): ?>
                                    <?php $isExist = false;
                                    foreach ($dateFrom as $i => $dateOld) {
                                        if ($index == $dateOld) {
                                            $isExist = true;break;
                                        }
                                    } ?>
                                    <option <?php if ($isExist) echo "selected"; ?> value="<?php echo $index ?>"><?php echo $date ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span class="fa fa-calendar form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group form-item has-feedback">
                            <input value="<?php echo $budget ?>" placeholder="Budget" name="budget" type="text" class="form-control form-custom"
                                   id="field3"/>
                            <span class="fa fa-money form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <button id="btn-search" class="btn"><span class="fa fa-search"></span> Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>