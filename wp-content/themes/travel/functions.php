<?php
/**
 * @ Thiết lập các hằng dữ liệu quan trọng
 * @ THEME_URL = get_stylesheet_directory() - đường dẫn tới thư mục theme
 * @ CORE = thư mục /core của theme, chứa các file nguồn quan trọng.
 **/
define('THEME_URL', get_stylesheet_directory());
define('CORE', THEME_URL . '/core');
require_once(CORE . '/init.php');
session_start();
if (isset($_GET['cur'])) {
    $_SESSION['currency'] = trim($_GET['cur']);
}

require_once(CORE . '/widget.php');
require_once(CORE . '/currency_widget.php');

if (!isset($content_width)) {
    $content_width = 620;
}

if (!function_exists('setup_theme_travel')) {

    function setup_theme_travel()
    {
        $language_folder = THEME_URL . '/languages';
        load_theme_textdomain('travel', $language_folder);
        add_theme_support('post-thumbnails');
        add_theme_support('post-formats', array(
            'video',
            'image',
            'gallery',
            'quote',
            'link'
        ));
        add_theme_support('title-tag');
        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'travel'),
            'top_left' => __('Top menu left', 'travel'),
        ));
        $default_background = array(
            'default-color' => '#e8e8e8',
        );
        add_theme_support('custom-background', $default_background);

        $topLeftSideBar = array(
            'name' => __('Sidebar Menu Right', 'travel'),
            'id' => 'sidebar_menu_right',
            'description' => 'Sidebar Menu right for travel theme',
            'class' => 'sidebar_menu_right',
            'before_title' => '<h3 class="widgettitle">',
            'after_title' => '</h3>'
        );
        register_sidebar($topLeftSideBar);

        $sidebarone = array(
            'name' => __('Sidebar 1', 'travel'),
            'id' => 'sidebar_1',
            'description' => 'Sidebar 1 on right travel theme',
            'class' => 'sidebar_1',
            'before_title' => '<h3 class="widgettitle">',
            'after_title' => '</h3>'
        );
        register_sidebar($sidebarone);

        $sidebartwo = array(
            'name' => __('Sidebar 2', 'travel'),
            'id' => 'sidebar_2',
            'description' => 'Sidebar 2 on right travel theme',
            'class' => 'sidebar_2',
            'before_title' => '<h3 class="widgettitle">',
            'after_title' => '</h3>'
        );
        register_sidebar($sidebartwo);
    }

    add_action('init', 'setup_theme_travel');

}

if (!function_exists('setup_sidebar_single_page_travel')) {

    function setup_sidebar_single_page_travel()
    {
        $sidebarone = array(
            'name' => __('Sidebar single page 1', 'travel'),
            'id' => 'sidebar_single_1',
            'description' => 'Sidebar single page 1 on right',
            'class' => 'sidebar_single_1',
            'before_title' => '<h3 class="widgettitle">',
            'after_title' => '</h3>'
        );
        register_sidebar($sidebarone);

        $sidebartwo = array(
            'name' => __('Sidebar single page 2', 'travel'),
            'id' => 'sidebar_single_2',
            'description' => 'Sidebar single page 2 on right',
            'class' => 'sidebar_single_2',
            'before_title' => '<h3 class="widgettitle">',
            'after_title' => '</h3>'
        );
        register_sidebar($sidebartwo);
    }

    add_action('init', 'setup_sidebar_single_page_travel');
}


if (!function_exists('travel_header')) {
    function travel_header()
    {
        if (is_home()):
            printf('<h1><a href="%1$s"title="%2$s">%3$s</a></h1>', get_bloginfo('url'), get_bloginfo('description'), get_bloginfo('sitename'));
        else:
            printf('<p><a href="%1$s"title="%2$s">%3$s</a></p>', get_bloginfo('url'), get_bloginfo('description'), get_bloginfo('sitename'));
        endif;
    }
}

if (!function_exists('travel_menu')) {
    function travel_menu($menu)
    {
        $menu = array(
            'theme_location' => $menu,
            'container' => 'nav',
            'container_class' => $menu
        );
        wp_nav_menu($menu);
    }
}

if (!function_exists('travel_pagination')) {
    function travel_pagination()
    {
        /*
         * Không hiển thị phân trang nếu trang đó có ít hơn 2 trang
         */
        if ($GLOBALS['wp_query']->max_num_pages < 2) {
            return '';
        }
        ?>

        <nav class="pagination" role="navigation">
        <?php if (get_next_post_link()) : ?>
        <div class="prev"><?php next_posts_link(__('Older Posts', 'travel')); ?></div>
    <?php endif; ?>

        <?php if (get_previous_post_link()) : ?>
        <div class="next"><?php previous_posts_link(__('Newer Posts', 'travel')); ?></div>
    <?php endif; ?>

        </nav><?php
    }
}

/**
 * @ Hàm hiển thị ảnh thumbnail của post.
 * @ Ảnh thumbnail sẽ không được hiển thị trong trang single
 * @ Nhưng sẽ hiển thị trong single nếu post đó có format là Image
 * @ travel_thumbnail( $size )
 **/
if (!function_exists('travel_thumbnail')) {
    function travel_thumbnail($size)
    {

        // Chỉ hiển thumbnail với post không có mật khẩu
        if (!is_single() && has_post_thumbnail() && !post_password_required() || has_post_format('image')) :
            ?>
            <figure class="post-thumbnail"><?php the_post_thumbnail($size); ?></figure><?php
        endif;
    }
}

if (!function_exists('travel_entry_header')) {
    function travel_entry_header()
    {
        if (is_single()) : ?>

            <h1 class="entry-title">
                <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                    <?php the_title(); ?>
                </a>
            </h1>
        <?php else : ?>
            <h2 class="entry-title">
            <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?>
            </a>
            </h2><?php

        endif;
    }
}

if (!function_exists('travel_entry_meta')) {
    function travel_entry_meta()
    {
        if (!is_page()) :
            echo '<div class="entry-meta">';

            // Hiển thị tên tác giả, tên category và ngày tháng đăng bài
            printf(__('<span class="author">Posted by %1$s</span>', 'travel'),
                get_the_author());

            printf(__('<span class="date-published"> at %1$s</span>', 'travel'),
                get_the_date());

            printf(__('<span class="category"> in %1$s</span>', 'travel'),
                get_the_category_list(', '));

            // Hiển thị số đếm lượt bình luận
            if (comments_open()) :
                echo ' <span class="meta-reply">';
                comments_popup_link(
                    __('Leave a comment', 'travel'),
                    __('One comment', 'travel'),
                    __('% comments', 'travel'),
                    __('Read all comments', 'travel')
                );
                echo '</span>';
            endif;
            echo '</div>';
        endif;
    }
}

function travel_readmore()
{
    return '...<a class="read-more" href="' . get_permalink(get_the_ID()) . '">' . __('Read More', 'travel') . '</a>';
}

add_filter('excerpt_more', 'travel_readmore');


if (!function_exists('travel_entry_content')) {
    function travel_entry_content()
    {
        if (!is_single()) :
            the_excerpt();
        else :
            the_content();

            /*
             * Code hiển thị phân trang trong post type
             */
            $link_pages = array(
                'before' => __('<p>Page:', 'travel'),
                'after' => '</p>',
                'nextpagelink' => __('Next page', 'travel'),
                'previouspagelink' => __('Previous page', 'travel')
            );
            wp_link_pages($link_pages);
        endif;

    }
}

if (!function_exists('travel_entry_tag')) {
    function travel_entry_tag()
    {
        if (has_tag()) :
            echo '<div class="entry-tag">';
            printf(__('Tagged in %1$s', 'travel'), get_the_tag_list('', ', '));
            echo '</div>';
        endif;
    }
}

// custom header
$defaults = array(
    'default-image' => '',
    'width' => 0,
    'height' => 0,
    'flex-height' => false,
    'flex-width' => false,
    'uploads' => true,
    'random-default' => false,
    'header-text' => true,
    'default-text-color' => '',
    'wp-head-callback' => '',
    'admin-head-callback' => '',
    'admin-preview-callback' => '',
);
add_theme_support('custom-header', $defaults);


function get_image_header()
{
    $url = get_theme_mod('header_image', get_theme_support('custom-header', 'default-image'));
    if ('remove-header' == $url)
        return '';
    if (is_random_header_image())
        $url = get_random_header_image();
    return esc_url_raw(set_url_scheme($url));
}

// case Departure
$args = array(
    'hierarchical' => true,
    'label' => 'Departures',
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array(
        'slug' => 'departure'
    ),
    'singular_label' => 'Departure'
);
register_taxonomy('departure', array('post', 'holiday', 'flight'), $args);
$args = array(
    'hierarchical' => true,
    'label' => 'Destinations',
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array(
        'slug' => 'destination'
    ),
    'singular_label' => 'Destination'
);
register_taxonomy('destination', array('post', 'holiday', 'flight'), $args);

function getTaxonomy($name)
{
    $term_args = array(
        'taxonomy' => $name,
        'hide_empty' => false
    );
    $terms = get_terms($term_args);
    if (is_array($terms)) {
        return $terms;
    }
    return [];
}

function getListDateTravel()
{
    $months = array();
    $currentMonth = (int)date('m') + 1;

    for ($x = $currentMonth; $x < $currentMonth + 12; $x++) {
        $months[date('Y-m', mktime(0, 0, 0, $x, 1))] = date('F Y', mktime(0, 0, 0, $x, 1));
    }
    return $months;
}


function getQueryParamsValue($query)
{
    return $_GET[$query];
}

// Set custom size for thumbnail
set_post_thumbnail_size(409, 204);

// procress when submit form
$departureId = array();
$destinationId = array();
$dateFrom = [];
$budget = null;
$page = 1;

if (isset($_GET['departure_id'])) {
    $departureId = $_GET['departure_id'];
}
if (isset($_GET['destination_id'])) {
    $destinationId = $_GET['destination_id'];
}
if (isset($_GET['date_from'])) {
    $dateFrom = $_GET['date_from'];
}
if (isset($_GET['budget'])) {
    $budget = $_GET['budget'];
}
if (isset($_GET['page'])) {
    $page = $_GET['page'] >= 1 ? $_GET['page'] : 1;
}

$tax_query = array();
$meta_query = array();
if ($departureId) {
    $tax_query[] = array(
        'taxonomy' => 'departure',
        'field' => 'term_id',
        'terms' => $departureId,
    );
}

if ($destinationId) {
    $tax_query[] = array(
        'taxonomy' => 'destination',
        'field' => 'term_id',
        'terms' => $destinationId,
    );
}
if (!empty($dateFrom)) {
    $listCondition = array();
    foreach ($dateFrom as $dateMonth) {
        $dateFormat = date_create_from_format('Y-m', $dateMonth);
        $month = $dateFormat->format('m');
        $year = $dateFormat->format('Y');
        $firstMinute = mktime(0, 0, 0, $month, 1, $year);
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $lastMinute = mktime(23, 59, 59, $month, $days, $year);
        array_push($listCondition, array(
            'key' => "wpcf-flight_date",
            'value' => array($firstMinute, $lastMinute),
            'compare' => 'BETWEEN',
            'type' => 'NUMERIC'
        ));
        array_push($listCondition, array(
            'key' => 'wpcf-holiday_date',
            'value' => array($firstMinute, $lastMinute),
            'compare' => 'BETWEEN',
            'type' => 'NUMERIC'
        ));

        array_push($listCondition, array(
            'key' => "wpcf-acc_date",
            'value' => array($firstMinute, $lastMinute),
            'compare' => 'BETWEEN',
            'type' => 'NUMERIC'
        ));
        array_push($listCondition, array(
            'key' => 'wpcf-cruise_date',
            'value' => array($firstMinute, $lastMinute),
            'compare' => 'BETWEEN',
            'type' => 'NUMERIC'
        ));

        array_push($listCondition, array(
            'key' => 'wpcf-other_date',
            'value' => array($firstMinute, $lastMinute),
            'compare' => 'BETWEEN',
            'type' => 'NUMERIC'
        ));

    }
    if (!empty($listCondition)) {
        $listCondition['relation'] = 'OR';
        $meta_query[] = $listCondition;
    }

}

if ($budget) {
    $meta_query[] = array(
        'relation' => 'or',
        array(
            'key' => 'wpcf-holiday_price',
            'value' => $budget,
            'compare' => '<=',
            'type' => 'NUMERIC'
        ),
        array(
            'key' => 'wpcf-flight_price',
            'value' => $budget,
            'compare' => '<=',
            'type' => 'NUMERIC'
        ),
        array(
            'key' => 'wpcf-acc_price',
            'value' => $budget,
            'compare' => '<=',
            'type' => 'NUMERIC'
        ),
        array(
            'key' => 'wpcf-cruise_price',
            'value' => $budget,
            'compare' => '<=',
            'type' => 'NUMERIC'
        ),
        array(
            'key' => 'wpcf-other_price',
            'value' => $budget,
            'compare' => '<=',
            'type' => 'NUMERIC'
        )
    );
}

$numberPostPerPage = 4;
$args = array(
    'post_type' => array('holiday', 'flight', 'cruise', 'other', 'accomodation'),
    'tax_query' => $tax_query,
    'meta_query' => $meta_query,
    'orderby' => 'date',
    'order' => 'DESC',
    'posts_per_page' => $numberPostPerPage,
    'paged' => $page
);

$argQueryNumberPosts = array(
    'post_type' => array('holiday', 'flight', 'cruise', 'other', 'accomodation'),
    'tax_query' => $tax_query,
    'meta_query' => $meta_query,
);

$totalPost = (new WP_Query($argQueryNumberPosts))->post_count;
$numberPage = ceil($totalPost / $numberPostPerPage);
$loop = new WP_Query($args);


global $error_message;
$error_message = '';
function dlf_auth($username, $password, $remember)
{
    global $user;
    $creds = array();
    $creds['user_login'] = $username;
    $creds['user_password'] = $password;
    $creds['remember'] = $remember;
    $user = wp_signon($creds, false);
    if (is_wp_error($user)) {
        return $user->get_error_message();
    } else {
        wp_set_current_user($user->ID);
        return '';
    }
}

if (isset($_POST['submit_login']) && !empty($_POST['submit_login'])) {
    $error_message = dlf_auth(isset($_POST['log']) ? $_POST['log'] : '', isset($_POST['pwd']) ? $_POST['pwd'] : '', isset($_POST['rememberme']) ? true : false);
}

add_theme_support( 'html5', array(
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
) );


function twentyseventeen_scripts() {
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'twentyseventeen_scripts' );