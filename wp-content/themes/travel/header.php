<!doctype html>
<html <?php language_attributes() ?>></html>
<head>
    <meta charset="<?php bloginfo('charset') ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="pingback" href="<?php bloginfo('pingback_url') ?>">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/style.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/select2.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Economica" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="container">
    <div class="nav-top">
        <?php include('nav_top.php') ?>
    </div>
    <div class="logo text-center">
    <?php get_image_header() ?>
       <a href="<?php echo get_site_url(); ?>">
            <img id="logo" class="img img-circle" src="<?php echo get_image_header() ?>"  alt="" onerror="this.src='<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.jpg'">
       </a>
    </div>
    <div class="nav-mid">
        <?php get_template_part('nav') ?>
    </div>

