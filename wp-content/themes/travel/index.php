<?php get_header(); ?>
    <div id="content">
        <div class="search from-search">
            <div class="container text-center">
                <div class="row">
                    <div class="col-sm-12 col-md-12 ">
                        <?php include(locate_template('form_search.php')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="container data-content">
            <div class="row">
                <div class="col-md-9 list-content col-sm-12">
                        <?php include(locate_template('content.php'));  ?>
                </div>
                <div class="col-md-3 sidebar col-sm-12">
                    <?php if (is_active_sidebar('sidebar_1')) : ?>
                        <div class="sidebar1" style="margin-bottom: 20px">
                            <div id="widget-area" class="widget-area" role="complementary">
                                <?php dynamic_sidebar('sidebar_1'); ?>
                            </div><!-- .widget-area -->
                        </div>
                    <?php endif; ?>

                    <?php if (is_active_sidebar('sidebar_2')) : ?>
                        <div class="sidebar2">
                            <div id="widget-area" class="widget-area" role="complementary">
                                <?php dynamic_sidebar('sidebar_2'); ?>
                            </div><!-- .widget-area -->
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>