$(function () {

    $('.national-money .current-lang a').attr('href', '#');
    $('.national-money .current-lang img').popover({
        html: true,
        placement: 'bottom',
        template: '<div  style="min-width:150px;width:auto"  class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>',
        content: $("#list-national-container").clone(false).html()
    });

    $('.national-money .current-lang img').on('show.bs.popover', function () {
        $('.img-icon-money').popover('hide');
    });

    $('.img-icon-money').on('show.bs.popover', function () {
        $('.national-money .current-lang img').popover('hide');
    });

    $('.img-icon-money').popover({
        html: true,
        placement: 'bottom',
        template: '<div  style="min-width:150px;width:auto"  class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        content: $("#list-money-container").clone(false).html()
    });

    if ($(window).width() < 768) {
        changeTypeDisplayNational('right');
        changeTypeDisplayMoney('right');
    }
    $(window).resize(function () {
        if ($(this).width() < 768) {
            changeTypeDisplayNational('right');
            changeTypeDisplayMoney('right');
        } else {
            changeTypeDisplayNational('bottom');
            changeTypeDisplayMoney('bottom');
        }
    });


    function changeTypeDisplayMoney(placement) {
        $('.img-icon-money').popover('destroy').popover({
            html: true,
            placement: placement,
            template: '<div  style="min-width:150px;width:auto"  class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
            content: $("#list-money-container").clone(false).html()
        });

        $('.img-icon-money').on('show.bs.popover', function () {
            $('.national-money .current-lang img').popover('hide');
        })
    }

    function changeTypeDisplayNational(placement) {
        $('.national-money .current-lang img').popover('destroy').popover({
            html: true,
            placement: placement,
            template: '<div  style="min-width:150px;width:auto"  class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>',
            content: $("#list-national-container").clone(false).html()
        });

        $('.national-money .current-lang img').on('show.bs.popover', function () {
            $('.img-icon-money').popover('hide');
        })
    }

    $('#departure').select2({
        placeholder: 'Departure'
    });
    $('#destination').select2({
        placeholder: 'Destination'
    });
    $('#date_from').select2({
        placeholder: 'Travel Priod'
    });

    $('body').css('visibility', 'initial');

    $('#showmore_flight').click(function (event) {
        event.preventDefault();
        $('.flight-item').show();
    });

    $('#showless_flight').click(function () {
        event.preventDefault();
        $('.flight_hide').hide();
    });
    $('#showmore_acc').click(function (event) {
        event.preventDefault();
        $('.acc-item').show();
    });

    $('#showless_acc').click(function () {
        event.preventDefault();
        $('.acc_hide').hide();
    });

    $('#login_trigger').click(function(){
       $('#login-dialog').toggle();
    });

});