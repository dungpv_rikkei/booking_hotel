<?php if ( has_nav_menu( 'primary' ) ) : ?>
    <nav class="bs-docs-nav navbar navbar-static-top" id="nav-mid">
            <?php
            // Primary navigation menu.
            wp_nav_menu( array(
                'menu_class'     => 'list-item',
                'theme_location' => 'primary',
            ) );
            ?>
    </nav>
<?php endif; ?>

