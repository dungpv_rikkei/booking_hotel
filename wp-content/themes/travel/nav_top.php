<nav class="navbar  navbar-default">

    <div class="container-fluid">
        <div class="navbar-header">
            <button aria-controls="bs-navbar" aria-expanded="false" class="navbar-toggle collapsed"
                    data-target="#bs-navbar" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
            </button>
            <a href="../" class="navbar-brand">Travel</a>
        </div>
        <div class="navbar-collapse collapse" id="bs-navbar" aria-expanded="false" style="height: 1px;">
            <?php if (has_nav_menu('top_left')) : ?>
                <?php
                // Primary navigation menu.
                wp_nav_menu(array(
                    'menu_class' => 'nav navbar-nav',
                    'theme_location' => 'top_left',
                ));
                ?>
            <?php endif; ?>
            <?php global $error_message;
            if (!is_user_logged_in()): ?>
            <ul class="nav navbar-nav menu-login">
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16532">
                    <a href="#" id="login_trigger">
                        <img style="margin-top: -7px"
                             src="<?php echo esc_url(get_template_directory_uri()); ?>/images/user.gif" alt="login">
                        <span class="login-title">Login</span>
                    </a>
                    <div id="login-dialog" style="display: none">
                        <form id="form-login" class="" method="post">
                            <?php if (!empty($error_message)): ?>
                                <div class="error-message-login">
                                    <?php echo $error_message; ?>
                                </div>
                            <?php endif; ?>
                            <div class="form-group form-username">
                                <label class="control-label label-login" for="username">Username:</label>
                                <input type="text" required class="form-control control-login" name="log"
                                       placeholder="Username">
                            </div>
                            <div class="form-group">
                                <label class="control-label label-login" for="password">Password:</label>
                                <input required type="password" class="form-control control-login" name="pwd"
                                       placeholder="Password">
                            </div>

                            <div class="checkbox">
                                <label class="checkbox-remember"><input name="rememberme" value="1"
                                                                        type="checkbox"><span
                                            class="label-remember">Remember me</span></label>
                            </div>
                            <input type="hidden" value="1" name="submit_login" id="submit-login">
                            <input type="hidden" value="1" name="submit_register"  id="submit_register">
                            <button  type="submit" class="btn btn-booking btn-login">LOG IN</button>
                            <button type="button" onclick="location.href ='<?php echo site_url() ?>/wp-login.php?action=register'"  class="btn btn-booking btn-register">REGISTER</button>
                        </form>
                    </div>
                </li>
            </ul>
            <?php endif; ?>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Tiếng Việt <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
</nav>


