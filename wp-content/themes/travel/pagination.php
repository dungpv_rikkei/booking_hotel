<?php
$queryString =  $_SERVER['QUERY_STRING'];
$postFix = '?';
if(strpos($queryString,'&page')){
    $queryString = preg_replace("/&page=[0-9]*/",'',$queryString);
}else{
    $queryString = preg_replace("/page=[0-9]*/",'',$queryString);
}
$queryString = $postFix.$queryString;
$page = isset($_GET['page']) ? $_GET['page'] : 1;
$numberPageShowPagi = 5; ?>
<nav aria-label="Page navigation" class="text-center nav-pagination">
    <?php if ($numberPage > 1): ?>
        <ul class="pagination">
            <?php if ($numberPage <= $numberPageShowPagi) { ?>
                <?php for ($i = 1; $i <= $numberPage; $i++) { ?>
                    <li <?php if ($page == $i) {
                        echo 'class="active"';

                    } ?>><a href="<?php echo $queryString ?>&page=<?php echo $i ?>"><?php echo $i; ?></a>
                    </li>
                <?php } ?>

            <?php } else { ?>
                <?php if ($page > 1): ?>
                    <li>
                        <a href="<?php echo $queryString ?>&page=<?php echo $page - 1; ?>" class='pagi-left' aria-label="Previous first">
                            <span aria-hidden="true"><</span>
                        </a>
                    </li>
                <?php endif; ?>

                <?php
                if (($page + $numberPageShowPagi) > $numberPage) {
                    $offset = $numberPage;
                }else {
                    $offset = $numberPageShowPagi;
                }
                for ($i = $page; $i <= $offset; $i++) { ?>
                    <li <?php if ($page == $i) {
                        echo 'class="active"';
                    } ?>><a href="<?php echo $queryString ?>&page=<?php echo $i ?>"><?php echo $i; ?></a>
                    </li>
                <?php } ?>
                <?php if ($page < $numberPage): ?>
                    <li>
                        <a class='pagi-right' href="<?php echo $queryString ?>&page=<?php echo $page + 1; ?>" aria-label="Next">
                            <span aria-hidden="true">></span>
                        </a>
                    </li>
                <?php endif; ?>
            <?php } ?>
        </ul>
    <?php endif; ?>
</nav>
