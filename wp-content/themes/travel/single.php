<?php get_header(); ?>
<div class="carousel-inner container-fluid" style="margin-top: -20px">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <div class="background_header text-center">
                <img style="width: 100%;" class="img"
                     onerror="this.src='<?php echo esc_url(get_template_directory_uri()) . '/images/bearch_city.jpg'; ?>'"
                     src="<?php $src = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                     if (!empty($src)) echo $src ?>" alt="">
                <div class="container title_article_post">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 text-left col-xs-12">
                            <p><?php the_title() ?></p>
                        </div>
                        <div class="col-sm-6 col-xs-12 col-md-4 text-left">
                            <div class="list_heart">
                                <span class="fa fa-heart-o" aria-hidden="true"></span>
                                <span class="fa fa-heart-o" aria-hidden="true"></span>
                                <span class="fa fa-heart-o" aria-hidden="true"></span>
                                <span class="fa fa-heart-o" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="data-content main-content-container">
                <div class="row top_content">
                    <ul>
                        <li>
                    <span class="fa fa-user-o" aria-hidden="true">
                        <strong>
                              <?php
                              $meta_values = '';
                              if ('holiday' == get_post_type()) {
                                  $meta_values = get_post_meta($post->ID, 'wpcf-holiday_people_number', true);
                              } else if ('flight' == get_post_type()) {
                                  $meta_values = get_post_meta($post->ID, 'wpcf-flight_people_number', true);
                              } else if ('accomodation' == get_post_type()) {
                                  $meta_values = get_post_meta($post->ID, 'wpcf-acc_people_number', true);
                              } else if ('cruise' == get_post_type()) {
                                  $meta_values = get_post_meta($post->ID, 'wpcf-cruise_people_number', true);
                              } else if ('other' == get_post_type()) {
                                  $meta_values = get_post_meta($post->ID, 'wpcf-other_people_number', true);
                              }
                              echo $meta_values ? $meta_values : '0';
                              ?>
                        </strong>
                    </span>
                        </li>
                        <li>
                    <span class="fa fa-calendar icon2" aria-hidden="true">
                        <strong>
                            <?php
                            $meta_values = '';
                            if ('holiday' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-holiday_date', true);
                            } else if ('flight' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-flight_date', true);
                            } else if ('accomodation' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-acc_date', true);
                            } else if ('cruise' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-cruise_date', true);
                            } else if ('other' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-other_date', true);
                            }
                            echo $meta_values ? strtoupper(date("d.M.y", $meta_values)) : '';
                            ?>
                        </strong>
                    </span>
                        </li>
                        <li>
                            <?php
                            $meta_values = '';
                            if ('holiday' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-holiday_flight_price', true);
                            } else if ('flight' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-flight_price', true);
                            } else if ('accomodation' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-acc_flight_price', true);
                            } else if ('cruise' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-cruise_flight_price', true);
                            } else if ('other' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-other_flight_price', true);
                            }

                            ?>
                            <?php if (!empty($meta_values)): ?>
                                <span class="fa fa-plane icon3" aria-hidden="true">
                        <strong>
                            <?php echo $meta_values ? '$' . $meta_values : '$0'; ?>
                        </strong>
                    </span>
                            <?php endif; ?>
                        </li>
                        <li>
                            <?php
                            $meta_values = '';
                            if ('holiday' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-holiday_acc_price', true);
                            } else if ('flight' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-flight_acc_price', true);
                            } else if ('accomodation' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-acc_price', true);
                            } else if ('cruise' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-cruise_acc_price', true);
                            } else if ('other' == get_post_type()) {
                                $meta_values = get_post_meta($post->ID, 'wpcf-other_acc_price', true);
                            }
                            ?>
                            <?php if (!empty($meta_values)): ?>
                                <span class="fa fa-university icon4" aria-hidden="true">
                        <strong>
                            <?php echo $meta_values ? '$' . $meta_values : 'NO'; ?>
                        </strong>
                    </span>
                            <?php endif; ?>
                        </li>
                        <li>Total:
                            <?php
                            $totalPrice = 0;
                            if ('cruise' == get_post_type()) {
                                $totalPrice += intval(get_post_meta($post->ID, 'wpcf-cruise_price', true));
                            } else if ('other' == get_post_type()) {
                                $totalPrice += intval(get_post_meta($post->ID, 'wpcf-other_price', true));
                            } else if ('holiday' == get_post_type()) {
                                $totalPrice += intval(get_post_meta($post->ID, 'wpcf-holiday_price', true));
                            } else if ('flight' == get_post_type()) {
                                $totalPrice += intval(get_post_meta($post->ID, 'wpcf-flight_price', true));
                            } else if ('accomodation' == get_post_type()) {
                                $totalPrice += intval(get_post_meta($post->ID, 'wpcf-acc_price', true));
                            }
                            echo '$' . $totalPrice;
                            ?>
                        </li>
                    </ul>
                </div>
                <div class=" middle_content">
                    <div class="mid-content row-fluid">
                        <?php if (in_array(get_post_type(), array('holiday', 'flight', 'accomodation'))): ?>
                            <?php

                            $listFlight = array();
                            $listFlightLink = array();
                            if (in_array(get_post_type(), ['holiday', 'flight'])) {
                                $date = get_post_meta($post->ID, 'wpcf-holiday_flight_date', true);
                                if ($date) {
                                    $price = get_post_meta($post->ID, 'wpcf-holiday_flight_price', true);
                                    if ($price > 0) {
                                        $listFlight[intval(date('Ymd', $date))] = $price;
                                        $listFlightLink[intval(date('Ymd', $date))] = get_post_meta($post->ID, 'wpcf-holiday_flight_link', true);;
                                    }
                                } else {
                                    $date = get_post_meta($post->ID, 'wpcf-flight_date', true);
                                    if ($date) {
                                        $price = get_post_meta($post->ID, 'wpcf-flight_price', true);
                                        if ($price > 0) {
                                            $listFlight[intval(date('Ymd', $date))] = $price;
                                            $listFlightLink[intval(date('Ymd', $date))] = get_post_meta($post->ID, 'wpcf-flight_link', true);;
                                        }
                                    }
                                }
                            }

                            $listAccomodation = array();
                            $listAccomodationLink = array();
                            if (in_array(get_post_type(), ['holiday', 'accomodation'])) {
                                $date = get_post_meta($post->ID, 'wpcf-holiday_acc_date', true);
                                if ($date) {
                                    $price = get_post_meta($post->ID, 'wpcf-holiday_acc_price', true);
                                    if ($price > 0) {
                                        $listAccomodation[intval(date('Ymd', $date))] = $price;
                                        $listAccomodationLink[intval(date('Ymd', $date))] = get_post_meta($post->ID, 'wpcf-holiday_acc_link', true);
                                    }
                                } else {
                                    $date = get_post_meta($post->ID, 'wpcf-acc_date', true);
                                    if ($date) {
                                        $price = get_post_meta($post->ID, 'wpcf-acc_price', true);
                                        if ($price > 0) {
                                            $listAccomodation[intval(date('Ymd', $date))] = $price;
                                            $listAccomodationLink[intval(date('Ymd', $date))] = get_post_meta($post->ID, 'wpcf-acc_link', true);
                                        }
                                    }
                                }
                            }
                            $termDepatures = get_the_terms($post->ID, 'departure');
                            $termDestinations = get_the_terms($post->ID, 'destination');
                            $listDepartureId = array();
                            $listDestinationId = array();
                            foreach ($termDepatures as $depature) {
                                array_push($listDepartureId, $depature->term_id);
                            }

                            foreach ($termDestinations as $destination) {
                                array_push($listDestinationId, $destination->term_id);
                            }
                            if (!empty($listDestinationId) && !empty($listDepartureId)) {
                                $tax_query[] = array(
                                    'taxonomy' => 'departure',
                                    'field' => 'term_id',
                                    'terms' => $listDepartureId,
                                );
                                $tax_query[] = array(
                                    'taxonomy' => 'destination',
                                    'field' => 'term_id',
                                    'terms' => $listDestinationId,
                                );
                                $args = array(
                                    'post__not_in' => array($post->ID),
                                    'post_type' => array('holiday', 'flight'),
                                    'tax_query' => $tax_query,
                                );
                                $loop = new WP_Query($args);
                                // only show flight link with holiday and flight
                                if (in_array(get_post_type(), ['holiday', 'flight'])) {
                                    if ($loop->have_posts()) {
                                        while ($loop->have_posts()) : $loop->the_post();
                                            $date = get_post_meta(get_the_ID(), 'wpcf-holiday_flight_date', true);
                                            if ($date) {
                                                $price = get_post_meta(get_the_ID(), 'wpcf-holiday_flight_price', true);
                                                if ($price > 0) {
                                                    $dateConvert = intval(date('Ymd', $date));
                                                    if (isset($listFlight[$dateConvert])) {
                                                        if ($listFlight[$dateConvert] > $price) {
                                                            $listFlight[$dateConvert] = $price;
                                                            $listFlightLink[$dateConvert] = get_post_meta(get_the_ID(), 'wpcf-holiday_flight_link', true);
                                                        }
                                                    } else {
                                                        $listFlight[$dateConvert] = $price;
                                                        $listFlightLink[$dateConvert] = get_post_meta(get_the_ID(), 'wpcf-holiday_flight_link', true);
                                                    }
                                                }
                                            } else {
                                                $date = get_post_meta(get_the_ID(), 'wpcf-flight_date', true);
                                                if ($date) {
                                                    $price = get_post_meta(get_the_ID(), 'wpcf-flight_price', true);
                                                    $dateConvert = intval(date('Ymd', $date));
                                                    if ($price > 0) {
                                                        if (isset($listFlight[$dateConvert])) {
                                                            if ($listFlight[$dateConvert] > $price) {
                                                                $listFlight[$dateConvert] = $price;
                                                                $listFlightLink[$dateConvert] = get_post_meta(get_the_ID(), 'wpcf-flight_link', true);
                                                            }
                                                        } else {
                                                            $listFlight[$dateConvert] = $price;
                                                            $listFlightLink[$dateConvert] = get_post_meta(get_the_ID(), 'wpcf-flight_link', true);
                                                        }
                                                    }
                                                }
                                            }
                                        endwhile;
                                    }
                                }

                                $args = array(
                                    'post__not_in' => array($post->ID),
                                    'post_type' => array('holiday', 'accomodation'),
                                    'tax_query' => $tax_query,
                                );
                                $loop = new WP_Query($args);
                                if ($loop->have_posts()) {
                                    while ($loop->have_posts()) : $loop->the_post();
                                        $date = get_post_meta(get_the_ID(), 'wpcf-holiday_acc_date', true);
                                        if ($date) {
                                            $price = get_post_meta(get_the_ID(), 'wpcf-holiday_acc_price', true);
                                            if ($price > 0) {
                                                $dateConvert = intval(date('Ymd', $date));
                                                if (isset($listAccomodation[$dateConvert])) {
                                                    if ($listAccomodation[$dateConvert] > $price) {
                                                        $listAccomodation[$dateConvert] = $price;
                                                        $listAccomodationLink[$dateConvert] = get_post_meta(get_the_ID(), 'wpcf-holiday_acc_link', true);
                                                    }
                                                } else {
                                                    $listAccomodation[$dateConvert] = $price;
                                                    $listAccomodationLink[$dateConvert] = get_post_meta(get_the_ID(), 'wpcf-holiday_acc_link', true);
                                                }
                                            }
                                        } else {
                                            $date = get_post_meta(get_the_ID(), 'wpcf-acc_date', true);
                                            if ($date) {
                                                $price = get_post_meta(get_the_ID(), 'wpcf-acc_price', true);
                                                $dateConvert = intval(date('Ymd', $date));
                                                if ($price > 0) {
                                                    if (isset($listAccomodation[$dateConvert])) {
                                                        if ($listAccomodation[$dateConvert] > $price) {
                                                            $listAccomodation[$dateConvert] = $price;
                                                            $listAccomodationLink[$dateConvert] = get_post_meta(get_the_ID(), 'wpcf-acc_link', true);
                                                        }
                                                    } else {
                                                        $listAccomodation[$dateConvert] = $price;
                                                        $listAccomodationLink[$dateConvert] = get_post_meta(get_the_ID(), 'wpcf-acc_link', true);
                                                    }
                                                }
                                            }
                                        }
                                    endwhile;
                                }
                            }

                            //process flight date
                            ksort($listFlight);
                            ksort($listFlightLink);
                            $lastArrayDate = array();
                            $lastPrice = null;
                            $lastLink = null;
                            $listArrayDate = array();
                            $listArrayDateLink = array();
                            $listArrayPrice = array();
                            $count = count($listFlight);
                            foreach ($listFlight as $index => $flightPrice) {
                                if (empty($lastArrayDate)) {
                                    $lastArrayDate[] = $index;
                                    $lastPrice = $flightPrice;
                                    $lastLink = $listFlightLink[$index];
                                    continue;
                                }
                                if ($lastPrice == $flightPrice) {
                                    $lastArrayDate[] = $index;
                                } else {
                                    $text = '';
                                    $firstDate = $lastArrayDate[0];
                                    $lastDate = null;
                                    $countDate = count($lastArrayDate);
                                    if ($countDate >= 2) {
                                        $lastDate = $lastArrayDate[$countDate - 1];
                                    }
                                    $text = date_create_from_format('Ymd', $firstDate)->format('Y.d.m');
                                    if ($lastDate) {
                                        $text .= '-' . date_create_from_format('Ymd', $lastDate)->format('Y.d.m');
                                    }
                                    array_push($listArrayDate, $text);
                                    array_push($listArrayDateLink, $lastLink);
                                    array_push($listArrayPrice, $lastPrice);
                                    $lastArrayDate = array();
                                    $lastPrice = $flightPrice;
                                    $lastLink = $listFlightLink[$index];
                                    $lastArrayDate[] = $index;
                                }
                            }
                            if (!empty($lastArrayDate)) {
                                $text = '';
                                $firstDate = $lastArrayDate[0];
                                $lastDate = null;
                                $countDate = count($lastArrayDate);
                                if ($countDate >= 2) {
                                    $lastDate = $lastArrayDate[$countDate - 1];
                                }
                                $text = date_create_from_format('Ymd', $firstDate)->format('Y.d.m');
                                if ($lastDate) {
                                    $text .= '-' . date_create_from_format('Ymd', $lastDate)->format('Y.d.m');
                                }
                                array_push($listArrayDate, $text);
                                array_push($listArrayDateLink, $lastLink);
                                array_push($listArrayPrice, $lastPrice);
                            }


                            // process accomodation
                            ksort($listAccomodation);
                            ksort($listAccomodationLink);
                            $lastArrayDate = array();
                            $lastPrice = null;
                            $lastLink = null;
                            $listArrayDateAcc = array();
                            $listArrayDateLinkAcc = array();
                            $listArrayPriceAcc = array();

                            foreach ($listAccomodation as $index => $accPrice) {
                                if (empty($lastArrayDate)) {
                                    $lastArrayDate[] = $index;
                                    $lastPrice = $accPrice;
                                    $lastLink = $listAccomodationLink[$index];
                                    continue;
                                }
                                if ($lastPrice == $accPrice) {
                                    $lastArrayDate[] = $index;
                                } else {
                                    $text = '';
                                    $firstDate = $lastArrayDate[0];
                                    $lastDate = null;
                                    $countDate = count($lastArrayDate);
                                    if ($countDate >= 2) {
                                        $lastDate = $lastArrayDate[$countDate - 1];
                                    }
                                    $text = date_create_from_format('Ymd', $firstDate)->format('Y.d.m');
                                    if ($lastDate) {
                                        $text .= '-' . date_create_from_format('Ymd', $lastDate)->format('Y.d.m');
                                    }

                                    array_push($listArrayDateAcc, $text);
                                    array_push($listArrayDateLinkAcc, $lastLink);
                                    array_push($listArrayPriceAcc, $lastPrice);
                                    $lastArrayDate = array();
                                    $lastPrice = $accPrice;
                                    $lastLink = $$listAccomodationLink[$index];
                                    $lastArrayDate[] = $index;
                                }
                            }

                            if (!empty($lastArrayDate)) {
                                $text = '';
                                $firstDate = $lastArrayDate[0];
                                $lastDate = null;
                                $countDate = count($lastArrayDate);
                                if ($countDate >= 2) {
                                    $lastDate = $lastArrayDate[$countDate - 1];
                                }
                                $text = date_create_from_format('Ymd', $firstDate)->format('Y.d.m');
                                if ($lastDate) {
                                    $text .= '-' . date_create_from_format('Ymd', $lastDate)->format('Y.d.m');
                                }
                                array_push($listArrayDateAcc, $text);
                                array_push($listArrayDateLinkAcc, $lastLink);
                                array_push($listArrayPriceAcc, $lastPrice);
                            }

                            ?>
                            <div class="col-md-12 col-xs-12 col-sm-12 first_middle_content">
                                <?php
                                $classFlight = '';
                                if (count($listArrayDate) < 1) {
                                    $classFlight = 'hide';
                                } else {
                                    $classFlight = 'col-md-4 col-sm-4';
                                    if (count($listArrayDateAcc) < 1) {
                                        $classFlight = 'col-md-12 col-sm-12';
                                    }
                                }
                                ?>
                                <div class="<?php echo $classFlight ?>">
                                    <h3>Book a flight</h3>
                                    <p>
                                        <?php $i = 1;
                                        foreach ($listArrayDate as $index => $value): ?>
                                            <a <?php if ($i > 3) {
                                                echo 'style="display:none"';
                                            } ?> href="<?php echo $listArrayDateLink[$index] ?>"
                                                 class="flight-item <?php if ($i > 3) {
                                                     echo 'flight_hide';
                                                 } ?> "> <span><?php echo $value; ?> - <i class="fa fa-usd"
                                                                                          aria-hidden="true"><?php echo $listArrayPrice[$index]; ?></i></span><br/>
                                            </a>
                                            <?php $i++; endforeach; ?>
                                        <?php if (count($listFlight) > 3): ?>
                                            <span>
                                            <a href="#" id="showmore_flight"><i class="fa fa-caret-down"
                                                                                aria-hidden="true"></i> MORE/</a>
                                            <a href="#" id="showless_flight"><i class="fa fa-caret-up"
                                                                                aria-hidden="true"></i> LESS</a>
                                        </span>
                                        <?php endif; ?>
                                    </p>
                                </div>

                                <?php
                                $classPrice = '';
                                if (count($listArrayDateAcc) < 1) {
                                    $classPrice = 'hide';
                                } else {
                                    $classPrice = 'col-md-8 col-sm-8';
                                    if (count($listArrayDate) < 1) {
                                        $classPrice = 'col-md-12 col-sm-12';
                                    }
                                }
                                ?>

                                <div class="<?php echo $classPrice; ?>">
                                    <h3>Book an accomodation</h3>
                                    <p>
                                        <?php $i = 1;
                                        foreach ($listArrayDateAcc as $index => $value): ?>
                                            <a <?php if ($i > 3) {
                                                echo 'style="display:none"';
                                            } ?> href="<?php echo $listArrayDateLinkAcc[$index] ?>"
                                                 class="acc-item <?php if ($i > 3) {
                                                     echo 'acc_hide';
                                                 } ?> "> <span><?php echo $value; ?> - <i class="fa fa-usd"
                                                                                          aria-hidden="true"><?php echo $listArrayPriceAcc[$index]; ?></i></span><br/>
                                            </a>
                                            <?php $i++; endforeach; ?>
                                        <?php if (count($listAccomodation) > 3): ?>
                                            <span>
                                            <a href="#" id="showmore_acc"><i class="fa fa-caret-down"
                                                                             aria-hidden="true"></i> MORE/</a>
                                            <a href="#" id="showless_acc"><i class="fa fa-caret-up"
                                                                             aria-hidden="true"></i> LESS</a>
                                        </span>
                                        <?php endif; ?>
                                    </p>

                                    <?php if (empty($listAccomodation)): ?>
                                        <p class="text-justify">
                                            There is no accomodation option in this deal .If you need accomodation <a
                                                    href="#" style="color: red">here</A> is some opportunities to find
                                            your best accomodation
                                        </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-12 col-xs-12 col-sm-12 description-holiday">
                            <?php echo get_post_field('post_content', $post->ID) ?>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 finish_content comment">
                            <div class="comment list">
                                <?php
                                // If comments are open or we have at least one comment, load up the comment template.
                                if (comments_open() || get_comments_number()) :
                                    comments_template();
                                endif;
                                ?>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="col-md-2 siderbar-single-page">
            <?php if (is_active_sidebar('sidebar_single_1')) : ?>
                <div class="sidebar_single_1" style="margin-bottom: 20px">
                    <div id="widget-area" class="widget-area" role="complementary">
                        <?php dynamic_sidebar('sidebar_single_1'); ?>
                    </div><!-- .widget-area -->
                </div>
            <?php endif; ?>

            <?php if (is_active_sidebar('sidebar_single_2')) : ?>
                <div class="sidebar_single_2">
                    <div id="widget-area" class="widget-area" role="complementary">
                        <?php dynamic_sidebar('sidebar_single_2'); ?>
                    </div><!-- .widget-area -->
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer() ?>


